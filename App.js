import React from 'react';
import {NetInfo,Modal,View} from 'react-native';
import Text from 'react-native-text';
import RootStack from './src/components/navigation/RootStack';
import firebase from 'react-native-firebase';
import { NavigationActions } from 'react-navigation';
  
export default class App extends React.Component{
  constructor(props){
    super(props);
    navObject = {};
    firebase.admob().initialize('ca-app-pub-4436375490772881/3152196314')
    this.state = {connection:true};
  }
  hasPermission = async () => {
    let enabled = await firebase.messaging().hasPermission();
    if(enabled === 0){
      try {        
        await firebase.messaging().requestPermission({sound:true});        
        return new Promise(resolve => resolve(1));
      }catch(error){
        return new Promise(resolve => resolve(0));
      }
    }else{
      return new Promise(resolve => resolve(1));
    }
  }

  storeToken = (fcmToken,enabled) => {
    const collection = firebase.firestore().collection("users"); 
    collection.where('fcm_token','==',fcmToken).get((results) => {
      if(results.docs.length === 0){
        if(enabled === 1){firebase.messaging().subscribeToTopic("Updates")}
        firebase.firestore().collection("users").add({fcm_token : fcmToken,enabled:enabled});
      }else{
        firebase.firestore().collection("users").doc(results.docs[0].id).set({fcm_token : fcmToken,enabled:enabled});
      }
    });
    
  }
  
  deleteToken = (fcmToken) => {
    const collection = firebase.firestore().collection("users");
    collection.where('fcm_token','==',fcmToken).get((results) => {
      if(results.docs.length > 0){
        firebase.firestore().collection("users").doc(results.docs[0].id).delete().then(() => {
          firebase.messaging().unsubscribeFromTopic("Updates");
        });
      }        
    }); 
  }



  componentDidMount = () => {
    //For app closed    
    const navigate = NavigationActions.navigate({
      routeName : 'Updates'
    });

    let token = null;

    NetInfo.isConnected.addEventListener(
      'connectionChange',
      (connection) => (this.setState({connection:connection}))
    );

    //Below is the previously working condition except that sometimes token was null.
    /*this.hasPermission().then(enabled => {
      firebase.messaging().getToken().then((fcmToken) => {
        if(fcmToken !== null){
          this.storeToken(fcmToken,enabled);
        }
      });
    });*/
    
    //Below function is the new implementation to avoid null token.
  firebase.messaging().onTokenRefresh(fcmToken => {
      this.hasPermission().then(enabled => {
        if(token !== null){this.deleteToken(token)}
        this.storeToken(fcmToken,enabled);
        token = fcmToken;
      });
    });

    
    firebase.notifications().getInitialNotification().then((notification) => {
      if(notification){
        if(this.navObject.hasOwnProperty('dispatch')){
          this.navObject.dispatch(navigate);
        }
      }
    });
      
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      if(this.navObject.hasOwnProperty('dispatch')){
        this.navObject.dispatch(navigate);
      }
    });

    firebase.notifications().setBadge(0);
  }

    render(){
      const {connection} = this.state;
      const errorModal = <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={true}
        >
          <View style={{marginTop: 22,flex:1,justifyContent:'center',alignItems:'center'}}>
              <Text style={{fontSize:25}}>Error!</Text>
              <View>
                <Text>It seems that you are offline.Please connect to the internet and try again.</Text>
              </View>
            </View>
        </Modal>
      </View>
      ;
      return  connection === true ? <RootStack ref={(ref) => {this.navObject=ref}}/>:errorModal;
  }
}
