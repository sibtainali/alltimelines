import React from 'react';
import {View,Switch,StyleSheet,AsyncStorage,TouchableHighlight,Linking} from 'react-native';
import Text from 'react-native-text';
import { omit, has } from 'lodash';
import htmlEntityDecoder from 'html-encoder-decoder';
import {CheckBox} from 'react-native-elements';
export default class RowTimeLine extends React.Component {

    constructor(props){
        super(props);
        this.key = props.item.id;
        this.state = {owned:false,finished:false};
    }

    handleCheck = (checkId) => {
        const {item, timeline_id} = this.props;
            this.setState((state) => {
                AsyncStorage.getItem('timeline_items',(err,result) => {
                    const reducedItem = {id: item.id, bgcolor: item.bgcolor, media__type: item.media__type, timeline: item.timeline};                    
                    const other = checkId === 'owned'? 'finished': 'owned';

                    if(result === null){
                        AsyncStorage.setItem('timeline_items', JSON.stringify({[timeline_id]: {[checkId]: {[this.key]: reducedItem}, [other]: {}}}));
                    }else{
                        let parsedResults = JSON.parse(result);
                        if(has(parsedResults,timeline_id)){
                            let timeline = parsedResults[timeline_id];
                            timeline = !has(timeline,'owned') ? {...timeline, owned: {}}: timeline;
                            timeline = !has(timeline,'finished') ? {...timeline, finished: {}}: timeline;
                            if(state[checkId]){
                                timeline[checkId] =  omit(timeline[checkId],this.key);
                            }else{
                                timeline[checkId][this.key] = reducedItem;
                            }
                            AsyncStorage.setItem('timeline_items', JSON.stringify({ ...parsedResults, [timeline_id]: timeline}));
                        }else{
                            parsedResults = { ...parsedResults, [timeline_id]: {[checkId]: {[this.key]: reducedItem } }};
                            AsyncStorage.setItem('timeline_items', JSON.stringify({ ...parsedResults }));
                        }
                    }
                });

                return {[checkId]:!state[checkId]}
            });
    }

    componentDidMount = () => {
        const { timeline_id } = this.props;
        AsyncStorage.getItem('timeline_items').then((item) => {
            if(item !== null){
                const itemParsed = JSON.parse(item);
                const owned = has(itemParsed,[timeline_id,'owned', this.key]);
                const finished = has(itemParsed,[timeline_id,'finished', this.key]);
                this.setState(() => ({owned,finished}));
            }
        }).catch((e) => {
        });
    }

  render() {
      const {firstColumn,middleColumn,lastColumn,text} = styles;
      let {item,navigation} = this.props;
    return (
        <View style={{flex:1}}>
        <View style={{flexDirection:'row',backgroundColor:item.bgcolor}}>
            <View style={firstColumn}>
            <CheckBox uncheckedColor='white' checkedColor='#00A700' checked={this.state.owned} onPress={() => (this.handleCheck('owned'))}/>
            </View>
            <View style={middleColumn}>
            <TouchableHighlight onPress={() => {Linking.openURL(item.link)}}>
            <Text style={text}>{htmlEntityDecoder.decode(item.title.rendered)}</Text>
            </TouchableHighlight>
            </View>
            <View style={lastColumn}>
            <CheckBox checked={this.state.finished} uncheckedColor='white' checkedColor='#00A700' onPress={() => (this.handleCheck('finished'))}/>
            </View>
        </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    firstColumn:{
        flex:0.2,
        alignItems:'flex-start'
    },
    middleColumn:{
        flex:0.8,
        justifyContent:'center',
        paddingLeft:20,
        paddingRight:5
    },
    text : {
        color:'white',
        fontSize:18
    },
    lastColumn:{
        flex:0.2,
        alignItems:'flex-end'
    }
});