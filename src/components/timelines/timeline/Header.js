import React from 'react';
import {View,StyleSheet} from 'react-native';
import Text from 'react-native-text';
import Select from '../Select';
export default class Header extends React.Component {
    render(){
        const {header,heading,text,select} = styles;
        const {timeline,state,sortData,filterFunction,sortFunction, savedData, savedFunction} = this.props;
        const [mediaTypes,title,desc] = timeline;
        const filterData = mediaTypes.map((type,index) => ({key : 'filter_'+index,label : type.trim(),value : type.trim().toLowerCase()}));
        filterData.push({key:'filter_data',label : 'Everything',value:undefined});
        const {filter,sort, saved} = state;
        return(
        <View style={header}>
        <Text style={heading}>{title}</Text>
        <Text style={text}>{desc}</Text>
        <View style={select}>
            {Array.isArray(mediaTypes) && mediaTypes.length > 0 &&
                <Select style={{marginBottom:10}} multiple={true} value={filter} label='Filter' filterFunction={filterFunction} data={filterData}/>                
            }
                <Select style={{marginBottom:10}} value={sort} label='Sort' filterFunction={sortFunction} data={sortData}/>
                <Select style={{marginBottom:10}} value={saved} label='Hide' filterFunction={savedFunction} data={savedData}/>
        </View>
        </View>
        );
    }
}
const styles = StyleSheet.create({
    header:{
        marginTop:10,
    },
    select:{
        flexDirection:'row',
        flex:1,
        flexWrap:'wrap',
        justifyContent:'space-evenly',
        alignItems:'flex-start',
    },
    heading:{
        color:'white',
        marginTop:15,
        alignSelf:'center',
        fontSize:23,
        fontWeight:'bold'
    },
    text : {
        padding:20,
        color:'white',
        fontSize:18
    }
});