import React from 'react';
import QueryString from 'query-string';
import {Alert,View,StyleSheet,ActivityIndicator,Button,FlatList,AsyncStorage} from 'react-native';
import Text from 'react-native-text';
import { find, reduce, replace, has, keys } from 'lodash';
import RowTimeLine from '../RowTimeLine';
import Header from './Header';
import TableHeader from '../Header';
import Ads from '../../Ads';
import Interstitial from '../../Interstitial';

export default class TimeLine extends React.Component {
  static navigationOptions = {
    title: 'Timeline details',
  };
  constructor(props){
    super(props);
    const {params} = props.navigation.state;
    const sortData = [{key:'sort_'+1,label:'Chronological',meta:true,value : 'chronologicalorder',type : '_num'},{key:'sort_'+2,label:'Published Date',meta:true,value:'date_published',type:''},{key:'sort_'+3,label:'Name',meta:false,value:'title'}];
    const savedData = [{key:'saved_'+1,label:'Owned',value : 'owned'},{key:'saved_'+2,label:'Finished',value:'finished'},{key:'saved_'+3,label:'Both',value:'both'},{key:'saved_'+4,label:'None',value:undefined}];
    this.tableHeader = {id:100023,cols:[{text:'Owned',flex:0.2},{text:'Title',flex:0.5},{text:'Finished',flex:0.3, style: {alignItems: "flex-end"}}]};
    const default_sort = find(sortData,{value: params.default_sort});
    const query = params ? {'timeline' : params.id ,'filter[media__type]' : undefined,'filter[meta_key]' : default_sort.value, 'filter[orderby]' : `meta_value${default_sort.type}`,'filter[order]' : 'asc','page' :1,'per_page' : 20, 'exlude': undefined}:{};
    Object.assign(this,{timelineId:params.id,timelineDesc:params.desc,mediaTypes:params.mediaTypes,title:params.title,clicks:0,sortData:sortData,savedData: savedData,query:query});
    this.state = {saved: 'None',loading:true,data:null,loadingMore:false,filter:[{key : 'filter_init',label : 'Everything',value : undefined}],sort:default_sort.label,moreData:true};
  }
  
  loadMore = () => {
      if(this.state.moreData){
      this.setState((state) => ({loadingMore:true}));
      this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline_items',(state,response,moreData) => ({moreData:moreData,data:[...state.data,...response],loadingMore:false}));
      }
    }

  filter = (item) => {
    this.query.page = 1;
    let value = [];
    item.forEach(element => {
      value.push(element.value);
    });
    this.query['filter[media__type]'] = value.join(',');
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline_items',(state,response,moreData) => {response.unshift(this.tableHeader);return {moreData:moreData,filter:item,loading:false,data:response}});
  }

  saved = ({value:type}) => {
    AsyncStorage.getItem('timeline_items',(err, result) => {
        if(result !== null){
            const parsedData = JSON.parse(result);
            if(type !== undefined && has(parsedData,[this.timelineId])){
                const timelineIds = parsedData[this.timelineId];
                let ids = [];
                if(type === 'both'){
                    const { owned = {}, finished = {} } = timelineIds;
                    ids = keys({...owned, ...finished});
                }else{
                    ids = keys(timelineIds[type]);
                }
                this.query.page = 1;
                this.query.exclude = ids.join(',');
            }else{
                this.query.exclude = undefined;
                this.query.page = undefined;
            }
            this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline_items',(state,response,moreData) => {response.unshift(this.tableHeader);return {moreData:moreData,loading:false,data:response}});
        }
    });
  }

  sort = (item) => {
    const {meta,value,type,label} = item;
    this.query.page = 1;
    if(meta){
        this.query['filter[meta_key]'] = value;
        this.query['filter[orderby]'] = 'meta_value'+type; 
    }else{
        this.query['filter[meta_key]'] = undefined;
        this.query['filter[orderby]'] = value;
    }
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline_items',(state,response,moreData) => {response.unshift(this.tableHeader);return {moreData:moreData,sort:label,loading:false,data:response}});
  }

  fetchData = (url,stateFunction) => {
    const addr = url+'?'+QueryString.stringify(this.query,{strict:false,encode:false});
    this.clicks++;
    if(this.clicks <=1){
        fetch(addr).then((response) => (response.json()).then((JSONResponse) => ([response,JSONResponse]))).then((response) => {
            this.pagesCount = response[0].headers.get('X-WP-TotalPages');
            this.setState((state) => {
                this.query.page++;
                const moreData = (this.query.page <= this.pagesCount) ? true:false;
                this.clicks = 0;
                response[1] = Array.isArray(response[1])?response[1]:null;
                return stateFunction(state,response[1],moreData);
            });
        });
    }

  }

  flatListItem = ({item,index}) => {
      if(index === 0){
        return <TableHeader columns={item.cols}/>
      }else{
        return <RowTimeLine timeline_id={this.timelineId} item={item}/>;
      }
  }

  componentDidMount(){
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline_items',(state,response,moreData) => {response.unshift(this.tableHeader);return {moreData:moreData,loading:false,data:response}});
  }

  
  render() {
    const {data,loading,loadingMore,moreData} = this.state;
    const {container,flatlistContainer,mainloader} = styles;
    const componentList = (
    <View style={container}>
        <View style={flatlistContainer}>
            <FlatList onEndReached={this.loadMore} onEndReachedThreshold={0.3} stickyHeaderIndices={[1]} ListHeaderComponent={<Header state={this.state} savedData={this.savedData} sortData={this.sortData} timeline={[this.mediaTypes,this.title,this.timelineDesc]} filterFunction={this.filter} sortFunction={this.sort} savedFunction={this.saved}/>} data={data} keyExtractor={(item) => (item.id.toString())} renderItem={this.flatListItem} ListFooterComponent={loadingMore?<ActivityIndicator/>:moreData ? <Button onPress={this.loadMore} title='Load More'/>:<Text style={{color:'white',fontSize:16,fontWeight:'bold',alignSelf:'center'}}>No more results to show</Text>}/>
            <Ads/>
            <Interstitial/>
        </View>
    </View>
    );
    return (
        <View style={container}>
            {loading?(<View style={mainloader}><ActivityIndicator/></View>):(componentList)}
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'#222222'
    },
    flatlistContainer:{
        flex:1
    },
    mainloader:{
        flex:1,
        justifyContent:'center'
    }
  });