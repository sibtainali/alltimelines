import React from 'react';
import {View,StyleSheet} from 'react-native';
import Text from 'react-native-text';
export default class Header extends React.Component {
    render() {
        const {container} = styles;
        const {columns} = this.props;
      return (
          <View style={container}>
            {columns.map(column =>{
                const style = column.hasOwnProperty("style") ? column.style : {};
                return (<View key={column.text} style={{flex:column.flex,alignItems:'flex-start',paddingVertical:5,...style}}>
                    <Text style={{color:'white',fontWeight:'bold',fontSize:18}} allowFontScaling={false}>{column.text}</Text>
                </View>);
            }
            )}
          </View>
      );
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor:'#222222',
        flexDirection:'row',
        paddingTop:10,
        paddingHorizontal:10
    }
});