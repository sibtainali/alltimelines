import React from 'react';
import {AsyncStorage,ActivityIndicator,Alert,Button,FlatList,View,StyleSheet} from 'react-native';
import Text from 'react-native-text';
import Row from '../Row';
import Header from '../Header';
import Ads from '../../Ads';

export default class MyTimeLines extends React.Component {
  static navigationOptions = {
    title: 'My timelines',
  };
  constructor(props){
    super(props);
    this.data = [];
    this.state = {loading:true,data:null,myTimelines:null};
  }

  componentDidMount = async () => {
   
    try{
      let data = await AsyncStorage.getItem('myTimelines');
      data = JSON.parse(data);
      if(data !== null && data.length>0){
        fetch('https://www.alltimelines.com/wp-json/wp/v2/timeline?orderBy=title&include='+data.join(',')).then((response) => (response.json())).then((JSONResponse) => {
          JSONResponse.unshift({id : 'tableHeader',cols : [{text:'Save',flex:0.2},{text:'Title',flex:0.8}]});
          this.setState({loading:false,data:JSONResponse,myTimelines:data});  
        });
      }else{
        this.setState({loading:false});
      }
    }catch(error){
    }
  }
  flatlistItem = ({item,index}) => {
    const {navigation} = this.props;
    const {myTimelines} = this.state;
    if(index === 0){
      return <Header columns={item.cols}/>;
    }else{
      return <Row navigation={navigation} myTimelines={myTimelines} item={item}/>;
    }
  }

  render() {
    const {navigation} = this.props;
    const {wrapper,container,mainloader,text} = styles;
    const {loading,data,loadingMore,myTimelines} = this.state;
    const message = (myTimelines === null) ? 'Go to Find a Timeline and select your favorite stories and franchises from the list to see them here.':'Tap on your favorite timelines below to track your collection and your progress.';    
  const componentList = (
      <View style={wrapper}>
        <View style={wrapper}>
          <FlatList stickyHeaderIndices={[1]} ListHeaderComponent={<View><Text style={text}>{message}</Text></View>} data={data} keyExtractor={(item) => (item.id.toString())} renderItem={this.flatlistItem}/>
          <Ads/>
        </View>
      </View>
    );
    return (
        <View style={container}>
          {loading?(<View style={mainloader}><ActivityIndicator/></View>):(componentList)}
        </View>
      );
  }

}

const styles = StyleSheet.create({
  container: {
    flex:1,
    paddingTop:10,
    backgroundColor:'#222222'
  },
  wrapper:{
    flex:1,
  },
  text:{
    color:'white',
    fontSize:18
  },
  mainloader:{
    flex:1,
    justifyContent:'center'
  }
});