import React from 'react';
import {View,Switch,StyleSheet,AsyncStorage,TouchableHighlight} from 'react-native';
import Text from 'react-native-text';
import htmlEntityDecoder from 'html-encoder-decoder';
import {CheckBox} from 'react-native-elements';
export default class Row extends React.Component {
    
    constructor(props){
        super(props);
        this.key = 'myTimelines';
        this.state = {checked:false};
    }

    handleCheck = async () => {
        let {item,myTimelines} = this.props;

        if(!this.state.checked && myTimelines.indexOf(item.id) === -1){
            myTimelines.push(item.id);
            await AsyncStorage.setItem(this.key,JSON.stringify(myTimelines));
            this.setState(() => ({checked:true}));
        }else{
            myTimelines.splice(myTimelines.indexOf(item.id), 1 );
            await AsyncStorage.setItem(this.key,JSON.stringify(myTimelines));
            this.setState(() => ({checked:false}));
        }
    }

    componentDidMount = () => {
        const {myTimelines,item} = this.props;
        if(myTimelines.indexOf(item.id) !== -1){
            this.setState(() => ({checked:true}));
        }
    }

  render() {
    const {container,textContainer,text,checkbox} = styles;
    let {item, item: {default_sort = 'chronologicalorder'},navigation} = this.props;
    default_sort = default_sort === 'chronologicalorder' || default_sort === 'date_published' || default_sort === 'title' ? default_sort: 'chronologicalorder';
    return (
        <View style={container}>
            <View style={checkbox}>
                <CheckBox uncheckedColor='white' checkedColor='#00A700' checked={this.state.checked} onPress={this.handleCheck}/>
            </View>
            <View style={textContainer}>
            <TouchableHighlight onPress={() => navigation.navigate('TimeLine',{default_sort ,title : item.name,desc:item.app_description,id:item.id,mediaTypes : item.hasOwnProperty('media_types') && item.media_types.length > 0 ? item.media_types.split(',') : [] })}>
            <Text style={text}>{htmlEntityDecoder.decode(item.name)}</Text>
            </TouchableHighlight>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container : {
        flexDirection:'row',
        flex:1,
        paddingHorizontal:10
    },
    textContainer : {
        flex:0.8,
        justifyContent:'center',
        alignItems:'flex-start',
    },
    checkbox:{
        flex:0.2,
        alignItems:'flex-start'
    },
    text : {
        fontSize:20,
        color:'white'
    }
});