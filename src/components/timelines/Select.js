import React from 'react';
import {View} from 'react-native';
import Text from 'react-native-text';
import Dropdown from 'react-native-modal-selector';
export default class Select extends React.Component {

    render(){
        const {value,label,filterFunction,data,style={}} = this.props;
        let multiple = this.props.hasOwnProperty('multiple')?this.props.multiple:false;
        return (
            <View {...style}>
                <Text allowFontScaling={false} style={{color:'white',fontWeight:'bold',marginBottom:3,alignSelf:'center'}}>{label}</Text>
                <Dropdown
                    multiple={multiple}
                    initValue={value}
                    selectTextStyle={{color:'white'}}
                    onChange={(options) => {filterFunction(options)}}
                    data={data}
                />
            </View>
        );
    }
}
