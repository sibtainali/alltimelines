import React from 'react';
import {ActivityIndicator,Alert,Button,FlatList,View,StyleSheet,AsyncStorage} from 'react-native';
import Text from 'react-native-text';
import Search from './Search';
import Select from '../Select';
import Row from '../Row';
import Header from '../Header';
import Ads from '../../Ads';

export default class FindTimeLines extends React.Component {
  constructor(props){
    super(props);
    this.tableHeader = {id:100023,cols:[{text:'Save',flex:0.2},{text:'Title',flex:0.8}]};
    this.state = {loading:true,loadingMore:false,data:null,moreData:true, group: 'All', groupData: [{value: undefined, label: 'All', key: 'group_data_init'}]};
    Object.assign(this,{nextPage:1,query:'',clicks:0});
  }
  static navigationOptions = {
    title: 'Find timelines',
  };
  search = (query) => {
    this.nextPage = 1;
    this.setState(() => ({loading:true}));
    this.query = '&filter[search]='+query;
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline',(state,response,moreData) => {response.unshift(this.tableHeader);return {loading:false,data:response,moreData:moreData}});
  }

  getGroupData = () => {
    fetch('https://www.alltimelines.com/wp-json/wp/v2/timeline_groups').then((response) => (response.json()).then((response) => {
      this.setState((state) => {
        const { groupData } = state;
        const responseData = [];
        response.map((data, index) => {
          responseData.push({value: data.slug, label: data.name, key: 'group_data_'+index});
        });
        return { ...state, groupData: [...groupData, ...responseData]};
      });
    }));
  }
  
  group = ({value, label}) => {
    if(value !== undefined){
      this.nextPage = 1;
      this.query = '&filter[meta_key]=group&filter[meta_compare]=LIKE&filter[meta_value]='+value;
    }else{
      this.nextPage = 1;
      this.query = '';
    }

    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline',(state,response,moreData) => {response.unshift(this.tableHeader);return {moreData:moreData,group:label,loading:false,data:response}});
  }

  loadMore = () => {
    this.setState((state) => ({loadingMore:true}));
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline',(state,response,moreData) => {return {data:[...state.data,...response],loadingMore:false,moreData:moreData}});
  }

  fetchData = (url,stateFunction) => {
    const addr = url+'?per_page=20&page='+this.nextPage+this.query;
    this.clicks++;
    if(this.clicks <=1){
      fetch(addr).then((response) => (response.json()).then((JSONResponse) => ([response,JSONResponse]))).then((response) => {
        this.pagesCount = response[0].headers.get('X-WP-TotalPages');
        this.setState((state) => {
          this.nextPage++;
          const moreData = (this.nextPage <= this.pagesCount) ? true:false;
          this.clicks = 0;
          return stateFunction(state,response[1],moreData);
        });
      });
    }
  }
  reset = () => {
    this.query = '';
    this.nextPage = 1;
    this.setState({loading:true});
    this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline',(state,response,moreData) => {response.unshift(this.tableHeader);return {loading:false,data:response,moreData:moreData}});
  }
  
  flatlistItem = ({item,index}) => {
    const {navigation} = this.props;
    const {myTimelines} = this.state;
    if(index === 0){
     return <Header columns={item.cols}/>;
    }else{
      return <Row item={item} myTimelines={myTimelines} navigation={navigation}/>;
    }
  }

  componentDidMount = async () => {
    try{
      let data = await AsyncStorage.getItem('myTimelines');
      const myTimelines = data !== null ? JSON.parse(data) : [];
      this.getGroupData();
      this.fetchData('https://www.alltimelines.com/wp-json/wp/v2/timeline',(state,response,moreData) => {response.unshift(this.tableHeader);return {loading:false,data:response,myTimelines:myTimelines,moreData:moreData}});
    }catch(error){
    }
  }
  
  render() {
    const {navigation} = this.props;
    const {flatlistContainer,container,footer,mainloader, select} = styles;
    const {loading,data,loadingMore,moreData,group,groupData} = this.state;
    const header = (<View><Search search={this.search} reset={this.reset}/><View style={select}><Select value={group} label='Group' filterFunction={this.group} data={groupData}/></View><Text style={{color:'white',fontSize:18,margin:10}}>Check off the box next to your favorite stories/franchises.</Text></View>);    
    const componentList = (
    <View style={container}>
      <View style={flatlistContainer}>
      <FlatList 
        data={data} 
        stickyHeaderIndices={[1]}
        ListHeaderComponent={header} 
        keyExtractor={(item) => (item.id.toString())} renderItem={this.flatlistItem}
        ListFooterComponent={loadingMore?<ActivityIndicator/>:<View style={footer}>{moreData ?<Button onPress={this.loadMore} title='Load More'/>:<Text style={{color:'white',fontSize:16,fontWeight:'bold',alignSelf:'center'}}>No more results to show</Text>}</View>}/>
      </View>
      <Ads/>
    </View>
    );
    
    return (
        <View style={container}>
          {loading?(<View style={mainloader}><ActivityIndicator/></View>):(componentList)}
        </View>
      );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'#222222'
  },
  select:{
    flexDirection:'row',
    flex:1,
    justifyContent:'center',
    margin:10,
  },
  flatlistContainer:{
    flex:1    
  },
  mainloader:{
    flex:1,
    justifyContent:'center'
  },
  footer:{
    margin:10
  }
});