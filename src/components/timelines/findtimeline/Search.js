import React from 'react';
import {View,StyleSheet} from 'react-native';
import SearchBar from 'react-native-search-bar';
export default class FindTimeLines extends React.Component {
    render(){
        const {header} = styles;
        return(
        <View style={header}>
        <SearchBar
            barTintColor='black'
            onFocus={() => {this.refs.searchBar.focus()}}
            ref='searchBar'
            showsCancelButton={false}
            cancelButtonText='Reset'
            tintColor='white'
            placeholder='Search'
            onSearchButtonPress={(query) => {this.refs.searchBar.unFocus();this.props.search(query)}}
            onCancelButtonPress={this.props.reset}
        /></View>
        );
    }
}
const styles = StyleSheet.create({
    header:{
        marginTop:10,
    }
});