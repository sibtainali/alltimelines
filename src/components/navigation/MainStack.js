import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import {StackNavigator} from 'react-navigation';
import Home from '../home/Home';
import Updates from '../updates/Updates';
import MyTimeLines from '../timelines/mytimeline/MyTimeLines';
import FindTimeLines from '../timelines/findtimeline/FindTimeLines';
import TimeLine from '../timelines/timeline/TimeLine';

const MainStack = StackNavigator(
  {
    Home: {
      screen: Home,
    },
    Updates: {
      screen: Updates,
    },
    FindTimeLines:{
      screen: FindTimeLines
    },
    MyTimeLines:{
      screen : MyTimeLines
    },
    TimeLine: {
      screen : TimeLine
    }
  },
  {
    initialRouteName: 'Home',
    navigationOptions: ({navigation}) => ({
      headerRight:(
          <View>
            <Icon name="navicon" size={30} onPress={() => {if(navigation.state.routeName !== 'Home'){navigation.navigate('Home')}}}>
            </Icon>
          </View>
      ),
      headerLeft:(
          <View>
            <Icon name="chevron-left" size={40} onPress={() => navigation.goBack()}>
            </Icon>
          </View>
      ),
    })
  }
);

export default MainStack;