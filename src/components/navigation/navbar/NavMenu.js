import React from 'react';
import { StyleSheet,View,Button} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import Section from '../../home/Section';
//FILE NOT USED ANYMORE DUE TO CLIENT'S DEMAND.
export default class NavMenu extends React.Component {
  render() {
    const {navigation} = this.props;
    const {container,close} = styles;
    return (
        <View style={container}>
          <View style={close}>
            <Icon name="close" size={25} onPress={() => navigation.goBack()}/>
          </View>
          <Section navigation={navigation} screen="Home" text="Home" bg="#222222"/>
          <Section navigation={navigation} screen="MyTimeLines" text="My Timelines" bg="#222222"/>
          <Section navigation={navigation} screen="FindTimeLines" text="Find Timelines" bg="#222222"/>
          <Section navigation={navigation} screen="Updates" text="Updates" bg="#222222"/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop:30,
    flex:1,
  },
  close:{
    flexDirection:'row',
    justifyContent:'flex-start'
  }
});