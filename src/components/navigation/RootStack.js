import React from 'react';
import {StackNavigator} from 'react-navigation';
import MainStack from './MainStack';
import NavMenu from './navbar/NavMenu';

const RootStack = StackNavigator({
    Main: {
      screen: MainStack,
    },
    Menu: {
      screen: NavMenu,
    },
  },
  {
    mode: 'modal',
    headerMode: 'none',
  }
  );
  
  
  export default RootStack;