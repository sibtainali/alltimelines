import React from 'react';
import { StyleSheet,View,ActivityIndicator,FlatList,Button,Alert} from 'react-native';
import Text from 'react-native-text';
import UpdateItem from './UpdateItem';
import {parseString} from 'react-native-xml2js';
import Video from 'react-native-video';
import Player from './Player';
import Interstitial from '../Interstitial';

export default class Updates extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {loading:true,data:null, video: {}};
  }
  static navigationOptions = {
    title: 'Updates',
  };

  setVideo = (video) => {
    this.setState({video});
  }

  componentDidMount(){
    fetch('https://www.alltimelines.com/blog/feed/').then((response) => response.text()).then((response) => {
      parseString(response,(err,data) => {
        this.setState((state) => ({loading:false,data:data.rss.channel[0].item}));
      });
    });
  }

  render() {
    const {container,activityindicator, player} = styles;
    const {loading,data, video: { videoURI = false, videoTitle = false } } = this.state; 
    const componentList = (<View style={container}><FlatList data={data} keyExtractor={(item,index) => (index.toString())} renderItem={({item,index}) => <UpdateItem setVideo={this.setVideo} post={item}/>}/><Player url={videoURI} title={videoTitle}/><Interstitial/></View>);
    return (loading?(<View style={activityindicator}><ActivityIndicator/></View>):(componentList));
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:10,
    backgroundColor:'#222222',
    flex:1
  },
  activityindicator: {
    flex:1,
    justifyContent:'center',
    backgroundColor:'#222222'
  },
  player: {
    width:'100%',
    position:'absolute',
    bottom:0,
    backgroundColor:"#fff"
  }
    
});