import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default class Player extends Component{
    constructor(props){
        super(props);
        this.state = {loaded: false, duration: "", playing: false, currentTime: false}
    }

    onLoad = (payload) => {
        const { duration } = payload;
        this.setState((state) => ({...state, loaded: true, duration: (duration/60).toFixed(2)}));
    }

    togglePlayer = () => {
        this.setState((state) => ({...state,playing: !state.playing}));
    }

    onProgress = (payload) => {
        const { currentTime } = payload;
        this.setState((state) => ({...state,currentTime: Math.round(currentTime)}));
    }

    componentDidUpdate(prevProps){
        if(prevProps.url !== this.props.url){
            this.setState({loaded: false, duration: "", playing:false, currentTime:false});
        }
    }

    playbar = () => {
        const { loaded, playing, duration, currentTime } = this.state;
        const { title } = this.props;
        const { playbar } = styles;
        return (
            <View style={ playbar }>
                { loaded ? 
                    <TouchableOpacity onPress={this.togglePlayer}>
                        <Text>
                            { title !== false ? <Icon name={ playing ? 'pause': 'play' }/>: null}&nbsp;&nbsp;
                            { currentTime !== false ? `${currentTime} s / ${duration} mins` : null }&nbsp;
                            { title }
                        </Text>
                    </TouchableOpacity>
                    :
                    <ActivityIndicator />
                }
            </View>
        );
    }

    render(){
        const { url } = this.props;
        const { playing, duration, currentTime } = this.state;
        const { player } = styles;
        return (
            <View display={!url ? true:false} style={player}>
                { url !== false && <Video source={{uri: url}} progressUpdateInterval={1000} onProgress={this.onProgress} paused={!playing} onLoad={this.onLoad} audioOnly={true}/>}
                { this.playbar() }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    player: {
        width: '100%',
        height: 45,
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
        padding:5,
    },
    playbar: {
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
    }
});