import React from 'react';
import {View,ActivityIndicator,StyleSheet,Linking,Dimensions,Button} from 'react-native';
import Text from 'react-native-text';
import htmlEntityDecoder from 'html-encoder-decoder';
import Image from 'react-native-scalable-image';
import stripHtml from 'sanitize-html-react';

export default class UpdateItem extends React.Component {
 
  render() {
    const {post, setVideo} = this.props;
    const {titleStyle,excerpt,componentContainer} = styles;
    const {description,title,link, enclosure = false } = post;
    const audio = enclosure !== false && enclosure[0].hasOwnProperty('$') ? enclosure[0].$: false;
    const match = description[0].match(/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/);
    const image = Array.isArray(match) && match.length >=2 ? match[1]:false;
    const descriptionSanitized = stripHtml(description[0].match(/<p>[\S\s]*?<\/p>/gi)[0],{allowedTags:[]});
    const titleDecoded = htmlEntityDecoder.decode(title[0]);   
    return (
      <View style={componentContainer}>
          <Text style={titleStyle} onPress={() => Linking.openURL(link[0])}>{titleDecoded}</Text>
          {image !==false && <Image width={.95*Dimensions.get('window').width} source={{uri: image}}/>}
          {audio !== false && <Button onPress={() => setVideo({videoURI: audio.url, videoTitle: titleDecoded})} title='Play' color='#fff' />}
          <Text style={excerpt}>{htmlEntityDecoder.decode(descriptionSanitized)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    componentContainer:{
        flex:1,
        borderBottomColor: '#8E8E8E',
        borderBottomWidth: StyleSheet.hairlineWidth,
        paddingHorizontal:20,
        marginBottom: 10,
        borderRadius: 5,
        alignItems:'center',
    },
    excerpt:{
      fontSize:17,
      margin:10,
      color:'white'
    },
    titleStyle: {
      fontSize:25,
      color:'white',
      margin:10      
    }
  });
