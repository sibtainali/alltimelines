import React from 'react';
import { StyleSheet,View,TouchableHighlight} from 'react-native';
import Text from 'react-native-text';

export default class Section extends React.Component {
  
  render() {
    const {content,section,link} = styles;
    const {text,bg,screen,navigation} = this.props;
    
    return (
        <View style={[section,{backgroundColor:bg}]}>
          <TouchableHighlight style={link} onPress={() => navigation.navigate(screen)}>
            <Text style={content}>
              {text}
            </Text>          
        </TouchableHighlight>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  link: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center'
  },
  content: {
    color:'#fff',
    fontSize:20
  },
  section: {
    flex:1,
    flexDirection:'row'
  }
});

