import React from 'react';
import {Linking,StyleSheet,View,Alert} from 'react-native';
import Section from './Section.js';
import Interstitial from '../Interstitial';
export default class App extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };
  constructor(props){
    super(props);
  }
  
  render() {
    const {container} = styles;
    return (
      <View style={container}>
        <Section navigation={this.props.navigation} screen="MyTimeLines" text="MY TIMELINES" bg="#ebb035"/>
        <Section navigation={this.props.navigation} screen="FindTimeLines" text="FIND A TIMELINE" bg="#218559"/>
        <Section navigation={this.props.navigation} screen="Updates" text="UPDATES" bg="#06a2cb"/>
        <Section screen="https://www.patreon.com/alltimelines" navigation={{navigate : (screen) => {Linking.openURL(screen)}}} text="SUPPORT ALL TIMELINES" bg="#dd1e2f"/>
        <Interstitial />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1
  }
});
