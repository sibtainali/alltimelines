import React from 'react';
import {View,StyleSheet} from 'react-native'; 
import Text from 'react-native-text';
import firebase from 'react-native-firebase';
const Banner = firebase.admob.Banner;
export default class Ads extends React.Component {
    constructor(props){
        super(props);
        this.adId = "ca-app-pub-4436375490772881/3152196314";
        this.state = {error:false};
    }
    handleError = (error) => {
        this.setState((state) => ({error:error}))
    }
    render(){
        const {container} = styles;
        const {error} = this.state;
        const component = error !== false ? null:<View style={container}>{<Banner size={"SMART_BANNER"} unitId={this.adId} onAdFailedToLoad={(error) => this.handleError(error)}/>}</View>;
        return component;
    }
}


const styles = StyleSheet.create({
    container: {
      paddingVertical:10,
    }
  });