import React from 'react';
import firebase from 'react-native-firebase';

export default class Interstitial extends React.Component {
    constructor(props){
        super(props);
        const advert = firebase.admob().interstitial("ca-app-pub-4436375490772881/9288453773");
        //ca-app-pub-3940256099942544/1033173712
        const AdRequest = firebase.admob.AdRequest;
        const request = new AdRequest();
        advert.loadAd(request.build());
        const probability = Math.floor((Math.random() * 100) + 1);
        setTimeout(() => {
            if (advert.isLoaded() && probability <= 33) {
              advert.show();
            }
          }, 5000);
    }

    render = () => {
        return null;
    }
}
